CREATE EXTENSION pgcrypto;

CREATE TYPE sex AS ENUM ('male', 'female', 'other');

CREATE TABLE persons (
	uuid uuid PRIMARY KEY DEFAULT gen_random_uuid(),
	survived boolean,
	passenger_class integer,
	name text,
	sex sex,
	age numeric,
	siblings_or_spouses_aboard integer,
	parents_or_children_aboard integer,
	fare numeric
);

COPY persons(survived,passenger_class,name,sex,age,siblings_or_spouses_aboard,parents_or_children_aboard,fare)
FROM '/docker-entrypoint-initdb.d/titanic.csv' DELIMITER ',' CSV HEADER;
