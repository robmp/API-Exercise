// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	"crypto/tls"
	"database/sql"
	"github.com/go-openapi/strfmt"
	"log"
	"net/http"
	"os"

	errors "github.com/go-openapi/errors"
	runtime "github.com/go-openapi/runtime"
	middleware "github.com/go-openapi/runtime/middleware"

	"gitlab.com/robmp/API-Exercise/models"
	"gitlab.com/robmp/API-Exercise/restapi/operations"

	_ "github.com/lib/pq"
)

var db *sql.DB

//go:generate swagger generate server --target ../../API-Exercise --name Titanic --spec ../swagger.yml

func configureFlags(api *operations.TitanicAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

func initDB(connStr string) {
	var err error
	db, err = sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}
	if err = db.Ping(); err != nil {
		log.Fatal(err)
	}
}

func addPerson(personData *models.PersonData) *models.Person {
	var uuid strfmt.UUID
	err := db.QueryRow(
		"INSERT INTO persons(survived, passenger_class, name, sex, age, siblings_or_spouses_aboard, parents_or_children_aboard, fare) "+
			"VALUES($1, $2, $3, $4, $5, $6, $7, $8) "+
			"RETURNING uuid",
		&personData.Survived, &personData.PassengerClass, &personData.Name, &personData.Sex, &personData.Age,
		&personData.SiblingsOrSpousesAboard, &personData.ParentsOrChildrenAboard, &personData.Fare,
	).Scan(&uuid)
	if err != nil {
		log.Println(err)
	}
	person, err := getPerson(uuid)
	if err != nil {
		log.Println(err)
	}
	return &person
}

func getPerson(uuid strfmt.UUID) (person models.Person, err error) {
	err = db.QueryRow(
		"SELECT uuid, survived, passenger_class, name, sex, age, siblings_or_spouses_aboard, parents_or_children_aboard, fare "+
			"FROM persons "+
			"WHERE uuid = $1", uuid,
	).Scan(
		&person.UUID, &person.PersonData.Survived, &person.PersonData.PassengerClass,
		&person.PersonData.Name, &person.PersonData.Sex, &person.PersonData.Age,
		&person.PersonData.SiblingsOrSpousesAboard, &person.PersonData.ParentsOrChildrenAboard,
		&person.PersonData.Fare,
	)
	if err != nil {
		log.Println(err)
	}
	return
}

func deletePerson(uuid strfmt.UUID) (err error) {
	res, err := db.Exec("DELETE FROM persons WHERE uuid = $1", uuid)
	if err != nil {
		log.Println(err)
		return
	}
	count, err := res.RowsAffected()
	if err != nil {
		log.Println(err)
		return
	}
	if count == 0 {
		err = errors.NotFound("not found: person %s", uuid)
	}
	return
}

func updatePerson(uuid strfmt.UUID, personData *models.PersonData) (err error) {
	res, err := db.Exec(
		"UPDATE persons "+
			"SET survived=$1, passenger_class=$2, name=$3, sex=$4, age=$5, "+
			"siblings_or_spouses_aboard=$6, parents_or_children_aboard=$7, fare=$8 "+
			"WHERE uuid = $9",
		&personData.Survived, &personData.PassengerClass, &personData.Name, &personData.Sex, &personData.Age,
		&personData.SiblingsOrSpousesAboard, &personData.ParentsOrChildrenAboard, &personData.Fare, uuid)
	if err != nil {
		log.Println(err)
		return
	}
	count, err := res.RowsAffected()
	if err != nil {
		log.Println(err)
		return
	}
	if count == 0 {
		err = errors.NotFound("not found: person %s", uuid)
	}
	return
}

func listPeople() (people models.People) {
	rows, err := db.Query(
		"SELECT uuid, survived, passenger_class, name, sex, age, siblings_or_spouses_aboard, parents_or_children_aboard, fare " +
			"FROM persons",
	)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	for rows.Next() {
		var person models.Person
		err := rows.Scan(
			&person.UUID, &person.PersonData.Survived, &person.PersonData.PassengerClass,
			&person.PersonData.Name, &person.PersonData.Sex, &person.PersonData.Age,
			&person.PersonData.SiblingsOrSpousesAboard, &person.PersonData.ParentsOrChildrenAboard,
			&person.PersonData.Fare,
		)
		if err != nil {
			log.Fatal(err)
		}
		people = append(people, &person)
	}
	if err = rows.Err(); err != nil {
		log.Fatal(err)
	}
	return
}

func configureAPI(api *operations.TitanicAPI) http.Handler {
	// configure the api here
	api.ServeError = errors.ServeError

	// Set your custom logger if needed. Default one is log.Printf
	// Expected interface func(string, ...interface{})
	//
	// Example:
	// api.Logger = log.Printf

	api.JSONConsumer = runtime.JSONConsumer()

	api.JSONProducer = runtime.JSONProducer()

	if connStr, exists := os.LookupEnv("PG_CONNSTR"); exists {
		initDB(connStr)
	} else {
		log.Fatal("PG_CONNSTR env variable is not set, please set it with a libpq compatible connection string.")
	}

	api.PeopleAddHandler = operations.PeopleAddHandlerFunc(func(params operations.PeopleAddParams) middleware.Responder {
		person := addPerson(params.Person)
		return operations.NewPeopleAddOK().WithPayload(person)
	})
	api.PeopleListHandler = operations.PeopleListHandlerFunc(func(params operations.PeopleListParams) middleware.Responder {
		return operations.NewPeopleListOK().WithPayload(listPeople())
	})
	api.PersonDeleteHandler = operations.PersonDeleteHandlerFunc(func(params operations.PersonDeleteParams) middleware.Responder {
		if err := deletePerson(params.UUID); err != nil {
			return operations.NewPersonDeleteNotFound()
		}
		return operations.NewPersonDeleteOK()
	})
	api.PersonGetHandler = operations.PersonGetHandlerFunc(func(params operations.PersonGetParams) middleware.Responder {
		person, err := getPerson(params.UUID)
		if err != nil {
			return operations.NewPersonGetNotFound()
		}
		return operations.NewPersonGetOK().WithPayload(&person)
	})
	api.PersonUpdateHandler = operations.PersonUpdateHandlerFunc(func(params operations.PersonUpdateParams) middleware.Responder {
		if err := updatePerson(params.UUID, params.Person); err != nil {
			return operations.NewPersonUpdateNotFound()
		}
		return operations.NewPersonUpdateOK()
	})

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix"
func configureServer(s *http.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	return handler
}
