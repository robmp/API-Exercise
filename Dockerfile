FROM golang:1.13.4-alpine3.10 AS builder
WORKDIR /app
COPY . .
RUN go get ./...
RUN GOOS=linux GOARCH=amd64 go install ./cmd/titanic-server/

FROM alpine:3.10
COPY --from=builder /go/bin/titanic-server /go/bin/titanic-server
EXPOSE 8080
USER nobody
CMD ["/go/bin/titanic-server", "--host", "0.0.0.0", "--port", "8080"]
